<?php get_header(); ?>
<div id="category" class="wrap">
	<div id="breadcrumb">
		<?php
			if(function_exists('bcn_display')){
				bcn_display();
			}
		?>

	</div>
	<div id="content">

		<h1><?php single_cat_title(_e('Artículos en ', 'dxc')); ?></h1>
		
		<?php if(have_posts()): while(have_posts()): the_post(); ?>
		<div class="post">
			<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<p class="meta">
				<span class="category">Publicado en <?php echo the_category(', '); ?>,</span>
				<span class="date">el <?php the_time('F j, Y'); ?></span>
			</p>
			<div class="post-content">
				<?php the_content('Continuar Leyendo'); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>

		<?php if(function_exists('wp_paginate')) { wp_paginate(); } ?>

	</div>

	<div id="sidebar">
		<?php get_sidebar('posts'); ?>
	</div>


</div>
<?php get_footer(); ?>
