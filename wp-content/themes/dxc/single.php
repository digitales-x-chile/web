<?php get_header(); ?>
<?php the_post(); ?>

<div id="post" class="wrap">
	<div id="breadcrumb">
		<?php
			if(function_exists('bcn_display')){
				bcn_display();
			}
		?>
	</div>
	<div id="content">
		<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
		<p class="meta">
			<span class="category">Publicado en <?php echo the_category(', '); ?>,</span>
			<span class="date">el <?php the_date(); ?></span>
		</p>
		<div class="post-content">
			<?php the_content(); ?>
		</div>
	</div>
	<div id="sidebar">
		<?php get_sidebar('posts'); ?>
	</div>
</div>

<?php get_footer(); ?>