<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/forms.css" type="text/css" media="screen" />
	
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>

</head>

<body>

<div id="network-nav">
	<ul>
		<li class="first"><a href="http://www.digitalesporchile.org/" title="Digitales por Chile">Digitales por Chile</a></li>
		<li><a href="http://www.chileayuda.com/" title="Chile Ayuda">Chile Ayuda</a></li>
		<li class="last"><a href="http://chile.crisiscommons.org/" title="CrisisCommons Chile">CrisisCommons Chile</a></li>
	</ul>		
</div>

<div id="wrap">

	<div id="header">
		<h1>
			<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
				<span>Digitales por Chile</span>
			</a>
		</h1>
		
		<div id="menu">
			<?php
				$args = array(
					'theme_location'   => 'sitemenu',
					'container'       => false, 
					'depth'           => 0,
				);
				wp_nav_menu($args);
			?> 
		</div>
	
	</div>
	
	<div id="nav">
		<?php
			$args = array(
				'theme_location'  => 'navigation',
				'container'       => false, 
				'depth'           => 0,
			);
			wp_nav_menu($args);
		?> 
		<!--
		<ul>
			<li class="active"><a href="#" title="Inicio">Inicio</a></li>
			<li><a href="#" title="Fund Raising">Fund Raising</a></li>
			<li><a href="#" title="Quiénes Somos">Quiénes Somos</a></li>
			<li><a href="#" title="Qué Estamos Haciendo">Qué Estamos Haciendo</a></li>
			<li><a href="#" title="Qué Hemos Hecho">Qué Hemos Hecho</a></li>
			<li><a href="#" title="Transparencia Digital">Transparencia Digital</a></li>
		</ul>
		-->
	</div>
	
	<div id="container">