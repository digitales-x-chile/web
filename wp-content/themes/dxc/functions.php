<?php

/**
 * Definición de Sidebars
 */
 
register_sidebar( array(
	'name' => __( 'Sidebar Principal', 'dxc'),
	'id' => 'main',
	'description' => __( 'Sidebar que se despliega por defecto en el sitio', 'dxc' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );


register_sidebar( array(
	'name' => __( 'Sidebar Noticias', 'dxc'),
	'id' => 'posts',
	'description' => __( 'Sidebar que se despliega al ver noticias', 'dxc' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

	
/** Thumbnail automático para cada Post */

add_theme_support( 'post-thumbnails' );

add_image_size( 'dxc_home_thumbnail' , 160, 80, true );

/* Menus de Navegación */

register_nav_menus( array(
	
	'sitemenu' => __('Menú de Sitio', 'dxc'),
	'navigation' => __('Menú de Navegación', 'dxc'),
));
