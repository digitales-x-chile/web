<?php
/**
 * The main template file.
 *
 * Template Name: Home
 */

get_header(); ?>

<div id="home" class="wrap">
	<div id="content">
		<?php the_post(); ?>

		<?php $video_id = get_post_meta($post->ID, 'video', true);?>
		<div class="video">
			<object width="510" height="320">
				<param name="movie" value="http://www.youtube.com/v/<?php echo $video_id; ?>?fs=1&amp;hl=en_US&amp;rel=0&amp;border=1"></param>
				<param name="allowFullScreen" value="true"></param>
				<param name="allowscriptaccess" value="always"></param>
				<param name="wmode" value="opaque" />
				<embed src="http://www.youtube.com/v/<?php echo $video_id; ?>?fs=1&amp;hl=en_US&amp;rel=0&amp;border=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="510" height="320" wmode="opaque"></embed>
			</object>
		</div>
		
		<div class="pitch">
			<h1><?php the_title(); ?></h1>
			
			<div class="text">
				<?php $more = 0; the_content(); ?>
			</div>
		</div>
	
	</div>
</div>

<div id="feed" class="wrap">
	<div id="feedcontent">
		<!--<h2>News and Updates</h2>-->
		<h2>Actualidad</h2>
		<a href="http://www.digitalesporchile.org/ver/noticias/"><h3 class="more">ver más ...</h3></a>
		<?php
		query_posts(array('posts_per_page' => 3));
		?>
		<ul>
			<?php while(have_posts()): the_post(); ?>
			<li>
				<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
				<p class="meta">
					<span class="category"><?php echo array_pop(get_the_category())->cat_name; ?></span>
					<span class="date"><?php the_time('F j, Y'); ?></span>
				</p>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('dxc_home_thumbnail'); ?></a>
				<?php the_excerpt(); ?>
			</li>
			<?php endwhile; ?>
		</ul>
	</div>
</div>

<?php get_footer(); ?>