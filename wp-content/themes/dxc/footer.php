	</div>
	
	<div id="footer">
	
		<div class="wrap">
		
			<div id="about">
			
				<h3>
					<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>">
						<span>Digitales por Chile</span>
					</a>
				</h3>
				
				<p class="contact">
					<?php $page = get_page_by_title(__('Contáctanos', 'dxc')); ?>
					<a href="<?php echo get_permalink($page->ID); ?>" title="<?php echo get_the_title($page->ID); ?>"><?php echo get_the_title($page->ID); ?></a>
				</p>
				<p class="about">
					<?php // _e('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', 'dxc'); ?>
				</p>
				
			</div>

			<div id="logos">
				<h2>Trabajando y apoyando a</h2>
				<ul>
					<li>
						<a href="http://www.rhok.org/" title="Random Hacks of Kindness"><img src="<?php bloginfo('template_directory'); ?>/img/logos_rhok.png" alt="Logo RHoK"/></a>
					</li>
					<li>
						<a href="http://ushahidi.com/" title="Ushahidi"><img src="<?php bloginfo('template_directory'); ?>/img/logos_ushahidi.png" alt="Logo Ushahidi"/></a>
					</li>
					<li>
						<a href="http://crisiscommons.org/" title="Crisis Commons"><img src="<?php bloginfo('template_directory'); ?>/img/logos_crisiscommons.png" alt="Logo Crisis Commons"/></a>
					</li>					
				</ul>
				<h2>Con el apoyo de</h2>
				<ul>
					<li>
                                                <a href="http://www.probono.cl/" title="Fundación Pro Bono - El Compromiso de los Abogados con la Comunidad"><img src="<?php bloginfo('template_directory'); ?>/img/logos_probono.png" alt="Fundación Pro Bono"/></a>
                                        </li>
				</ul>
			</div>

			<div id="links">
				<ul>
					<li class="mail"><?php _e('Para mayor información:', 'dxc'); ?> <a href="mailto:contacto@digitalesporchile.org" title="Enviar e-mail a contacto@digitalesporchile.org">contacto@digitalesporchile.org</a></li>
					<li class="cc"><?php _e('Esta obra es publicada bajo una licencia', 'dxc'); ?> <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/cl/" title="Creative Commons"><img src="<?php bloginfo('template_directory'); ?>/img/cc_80x15.png" alt="Creative Commons BY-NC-ND"></a>.</li>
					<li class="facebook"><a href="http://www.facebook.com/group.php?gid=131559093550560&ref=ts" title="Digitales por Chile - Facebook"><?php _e('Únetenos en', 'dxc'); ?> <img src="<?php bloginfo('template_directory'); ?>/img/facebook_16.png" alt="Facebook"/></a></li>
					<li class="twitter"><a href="http://twitter.com/digitalesxchile" title="Digitales por Chile - Twitter"><?php _e('Síguenos en', 'dxc'); ?> <img src="<?php bloginfo('template_directory'); ?>/img/twitter_16.png" alt="Twitter"/></a></li>
				</ul>
			</div>

		</div>
		
	</div>
		
</div>
</body>
</html>
